extends Node2D


func _ready():
	pass

func _process(delta):
	$FPS.set_text(str(Performance.get_monitor(Performance.TIME_FPS)))
	
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().reload_current_scene()

func _on_Button_pressed():
	$AnimationPlayer.play("wone_anim")
	$Button.hide()
